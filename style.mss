Map {
  background-color: #afc6e9;
}

#world {
  polygon-fill: #fff;
  polygon-opacity: 0.75;
  line-color: #ddd;
  line-width: 0.3;
}
