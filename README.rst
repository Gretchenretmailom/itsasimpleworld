
===================
It's a simple world
===================

Simplest `CartoCSS <https://cartocss.readthedocs.io/en/latest/>`__ style 
showing countries of the world.

This programm includes data from OpenStreetMap collaborators in the
file: `simplified-land-polygons.shp`. It is available
under the Open Data Commons Open Database License (ODbL).

Copyright and copyleft
----------------------

Copyright (c) 2020

 * Deutsches GeoForschungsZentrum <https://www.gfz-potsdam.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

See the `LICENSE <./LICENSE>`__ for the full license text.
